package org.litepal.util;

import ohos.data.rdb.ValuesBucket;

public class LiteValuesBucket extends ValuesBucket {
    public void put(String key,Object values){
        if (values == null){
            putNull(key);
        }else if (values instanceof Integer){
            putInteger(key,(Integer) values);
        }else if (values instanceof Long){
            putLong(key,(Long) values);
        }else if (values instanceof Float){
            putFloat(key,(Float) values);
        }else if (values instanceof Double){
            putDouble(key,(Double) values);
        }else if (values instanceof Boolean){
            putBoolean(key,(Boolean) values);
        }else if (values instanceof Short){
            putShort(key,(Short) values);
        }else if (values instanceof Byte){
            putByte(key,(Byte) values);
        }else if (values instanceof byte[]){
            putByteArray(key,(byte[]) values);
        }else {
            putString(key,(String) values);
        }
    }
    public void put(String key,String values){
        putString(key,values);
    }
    public void put(String key,Long values){
        putLong(key,values);
    }
    public void put(String key,Integer values){
        putInteger(key,values);
    }
    public void put(String key,Double values){
        putDouble(key,values);
    }
    public void put(String key,Float values){
        putFloat(key,values);
    }
    public void put(String key,Boolean values){
        putBoolean(key,values);
    }
    public void put(String key,Short values){
        putShort(key,values);
    }
    public void put(String key,Byte values){
        putByte(key,values);
    }
    public void put(String key,byte[] values){
        putByteArray(key,values);
    }
}
