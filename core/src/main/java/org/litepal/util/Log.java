package org.litepal.util;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class Log {

    private static final HiLogLabel HI_LOG_LABEL = new HiLogLabel(HiLog.LOG_APP,0x2200,"LOG_TAG");

    public static void d(String tag,String msg){
        HiLog.debug(HI_LOG_LABEL,"TAG: %{public}s; MSG: %{public}s",tag,msg);
    }

    public static void e(String tag,String msg,Exception e){
        HiLog.error(HI_LOG_LABEL,"TAG: %{public}s; MSG: %{public}s",tag,msg);
    }
    public static void e(Exception e){
        HiLog.error(HI_LOG_LABEL,"error: %{public}s",e.getMessage());
    }
    public static void w(String tag,String msg){
        HiLog.warn(HI_LOG_LABEL,"TAG: %{public}s; MSG: %{public}s",tag,msg);
    }
}
