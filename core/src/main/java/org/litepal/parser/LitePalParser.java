/*
 * Copyright (C)  Tony Green, LitePal Framework Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.litepal.parser;

import java.io.IOException;
import java.io.InputStream;


import ohos.global.resource.RawFileEntry;
import ohos.global.resource.ResourceManager;
import ohos.javax.xml.parsers.ParserConfigurationException;
import ohos.javax.xml.parsers.SAXParserFactory;
import ohos.javax.xml.stream.XMLInputFactory;
import ohos.javax.xml.stream.XMLStreamConstants;
import ohos.javax.xml.stream.XMLStreamException;
import ohos.javax.xml.stream.XMLStreamReader;
import ohos.org.xml.sax.InputSource;
import ohos.org.xml.sax.SAXException;
import ohos.org.xml.sax.XMLReader;
import org.litepal.LitePalApplication;
import org.litepal.exceptions.ParseConfigurationFileException;
import org.litepal.util.Const;


/**
 * The class is used to parse the litepal.xml file. There're three usual ways to
 * parse XML in harmony, SAX, Pull and DOM. LitePal use SAX as default option,
 * and DOM parser will be added soon.
 * 
 * @author Tony Green
 * @since 1.0
 */
public class LitePalParser {

    /**
     * Node name dbname.
     */
    static final String NODE_DB_NAME = "dbname";

    /**
     * Node name version.
     */
    static final String NODE_VERSION = "version";

    /**
     * Node name list. Currently not used.
     */
    static final String NODE_LIST = "list";

    /**
     * Node name mapping.
     */
    static final String NODE_MAPPING = "mapping";

    /**
     * Node name column case.
     */
    static final String NODE_CASES = "cases";

    /**
     * Node name column storage.
     */
    static final String NODE_STORAGE = "storage";

    /**
     * Attribute name value, for dbname and version node.
     */
    static final String ATTR_VALUE = "value";

    /**
     * Attribute name class, for mapping node.
     */
    static final String ATTR_CLASS = "class";

    /**
     * Store the parsed value of litepal.xml.
     */
    private static LitePalParser parser;

    /**
     * Analyze litepal.xml, and store the analyzed result in LitePalParser. Use
     * DomParse to parse the configuration file as default. SAXParser and
     * XmlPullParser is also optional, but not visible to developers.
     *
     * @return litePal config
     */
    public static LitePalConfig parseLitePalConfiguration() {
        if (parser == null) {
            parser = new LitePalParser();
        }
        return parser.usePullParse();
    }
    /**
     * Use SAXParser to parse the litepal.xml file. It will get the parsed
     * result from LitePalContentHandler and stored in the instance of
     * LitePalAttr.
     * 
     * Note while analyzing litepal.xml file, ParseConfigurationFileException
     * could be thrown. Be careful of writing litepal.xml file, or developer's
     * application may be crash.
     */
    private void useSAXParser() {
        LitePalContentHandler handler;
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            XMLReader xmlReader = factory.newSAXParser().getXMLReader();
            handler = new LitePalContentHandler();
            xmlReader.setContentHandler(handler);
            xmlReader.parse(new InputSource(getConfigInputStream()));
        } catch (SAXException e) {
            throw new ParseConfigurationFileException(
                    ParseConfigurationFileException.FILE_FORMAT_IS_NOT_CORRECT);
        } catch (ParserConfigurationException e) {
            throw new ParseConfigurationFileException(
                    ParseConfigurationFileException.PARSE_CONFIG_FAILED);
        } catch (IOException e) {
            throw new ParseConfigurationFileException(ParseConfigurationFileException.IO_EXCEPTION);
        }
    }

    /* *
     * Use XMLInputFactory to parse the litepal.xml file. It will store the result
     * in the instance of {@link LitePalAttr}.
     * 
     * Note while analyzing litepal.xml file, ParseConfigurationFileException
     * could be thrown. Be careful of writing litepal.xml file, or developer's
     * application may be crash.
     */
    private LitePalConfig usePullParse() {
        try {
            LitePalConfig litePalConfig = new LitePalConfig();
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader xmlPullParser = factory.createXMLStreamReader(getConfigInputStream());
//            xmlPullParser.setInput(getConfigInputStream(), "UTF-8");

            while (xmlPullParser.hasNext()) {
                if (xmlPullParser.next() == XMLStreamConstants.START_ELEMENT){
                    String nodeName = xmlPullParser.getLocalName();
                    if (NODE_DB_NAME.equals(nodeName)) {
                        String dbName = xmlPullParser.getAttributeValue("", ATTR_VALUE);
                        litePalConfig.setDbName(dbName);
                    } else if (NODE_VERSION.equals(nodeName)) {
                        String version = xmlPullParser.getAttributeValue("", ATTR_VALUE);
                        litePalConfig.setVersion(Integer.parseInt(version));
                    } else if (NODE_MAPPING.equals(nodeName)) {
                        String className = xmlPullParser.getAttributeValue("", ATTR_CLASS);
                        litePalConfig.addClassName(className);
                    } else if (NODE_CASES.equals(nodeName)) {
                        String cases = xmlPullParser.getAttributeValue("", ATTR_VALUE);
                        litePalConfig.setCases(cases);
                    } else if (NODE_STORAGE.equals(nodeName)) {
                        String storage = xmlPullParser.getAttributeValue("", ATTR_VALUE);
                        litePalConfig.setStorage(storage);
                    }
                }
            }
            xmlPullParser.close();
            return litePalConfig;
        } catch (XMLStreamException e) {
            throw new ParseConfigurationFileException(
                    ParseConfigurationFileException.FILE_FORMAT_IS_NOT_CORRECT);
        } catch (IOException e) {
            throw new ParseConfigurationFileException(ParseConfigurationFileException.IO_EXCEPTION);
        }
    }

    /**
     * Iterates all files in the root of assets folder. If find litepal.xml,
     * open this file and return the input stream. Or throw
     * ParseConfigurationFileException.
     * 
     * @return The input stream of litepal.xml.
     * @throws IOException io
     */
    private InputStream getConfigInputStream() throws IOException {
        ResourceManager resManager = LitePalApplication.getSContext().getResourceManager();
        RawFileEntry rawFileEntry = resManager.getRawFileEntry("resources/rawfile/" + Const.Config.CONFIGURATION_FILE_NAME);

        return rawFileEntry.openRawFile();
    }
}

