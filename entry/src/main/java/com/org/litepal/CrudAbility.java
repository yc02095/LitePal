package com.org.litepal;

import com.org.litepal.slice.crud.*;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class CrudAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(CrudAbilitySlice.class.getName());

        addActionRoute("action.CrudSave", SaveSampleAbilitySlice.class.getName());
        addActionRoute("action.CrudUpdate", UpdateSampleAbilitySlice.class.getName());
        addActionRoute("action.CrudDelete", DeleteSampleAbilitySlice.class.getName());
        addActionRoute("action.CrudQuery", QuerySampleAbilitySlice.class.getName());
    }
}
