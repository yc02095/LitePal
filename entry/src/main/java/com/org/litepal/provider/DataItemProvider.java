package com.org.litepal.provider;

import com.org.litepal.ResourceTable;
import ohos.agp.components.*;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class DataItemProvider extends RecycleItemProvider {
    private ArrayList<List<String>> list;
    private Context mContext;
    public DataItemProvider(ArrayList<List<String>> list,Context context){
        this.list = list;
        mContext = context;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return list.get(i).hashCode();
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        DirectionalLayout layout;

        if (component == null){
            layout = new DirectionalLayout(mContext);
        }else {
            layout = (DirectionalLayout) component;
        }
        layout.removeAllComponents();
        layout.setOrientation(Component.HORIZONTAL);
        List<String> dataList = list.get(i);
        for (String data: dataList){
            DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(250,50);
            Text text = new Text(mContext);
            text.setText(data);
            text.setTextAlignment(TextAlignment.VERTICAL_CENTER);
            layout.addComponent(text,layoutConfig);
        }
        return layout;
    }
}
