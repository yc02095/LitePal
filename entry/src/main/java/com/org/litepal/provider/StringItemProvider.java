package com.org.litepal.provider;

import com.org.litepal.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;

public class StringItemProvider extends RecycleItemProvider {
    private ArrayList<String> list;
    private Context mContext;
    public StringItemProvider(ArrayList<String> list,Context context){
        this.list = list;
        mContext = context;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return list.get(i).hashCode();
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component c = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_layout_string,componentContainer,false);
        Text text = (Text) c.findComponentById(ResourceTable.Id_text_1);

        text.setText(list.get(i));

        return c;
    }
}
