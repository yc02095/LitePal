package com.org.litepal;

import com.org.litepal.slice.aggregate.*;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class AggregateAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(AggregateAbilitySlice.class.getName());

        addActionRoute("action.aggregate.average", AverageSampleAbilitySlice.class.getName());
        addActionRoute("action.aggregate.count", CountSampleAbilitySlice.class.getName());
        addActionRoute("action.aggregate.max", MaxSampleAbilitySlice.class.getName());
        addActionRoute("action.aggregate.min", MinSampleAbilitySlice.class.getName());
        addActionRoute("action.aggregate.sum", SumSampleAbilitySlice.class.getName());
    }
}
