package com.org.litepal;

import com.org.litepal.slice.manager.*;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.wifi.WifiDevice;

public class ManagerAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ManagerAbilitySlice.class.getName());

        addActionRoute("action.ModelList", ModelListAbilitySlice.class.getName());
        addActionRoute("action.TableList", TableListAbilitySlice.class.getName());
        addActionRoute("action.ModelStruture", ModelStrutureAbilitySlice.class.getName());
        addActionRoute("action.TableStruture", TableStrutureAbilitySlice.class.getName());
    }
}
