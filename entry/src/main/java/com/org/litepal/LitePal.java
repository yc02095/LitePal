package com.org.litepal;

import ohos.aafwk.ability.AbilityPackage;

public class LitePal extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        org.litepal.LitePal.initialize(getApplicationContext());
    }
}
