package com.org.litepal.slice.aggregate;

import com.org.litepal.ResourceTable;
import com.org.litepal.model.Singer;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import org.litepal.LitePal;

public class SumSampleAbilitySlice  extends AbilitySlice {
    private int result = 0;
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_aggregate_sum);

        Button mCountBtn1 = (Button) findComponentById(ResourceTable.Id_sum_btn1);
        Button mCountBtn2 = (Button) findComponentById(ResourceTable.Id_sum_btn2);
        TextField mAgeEdit = (TextField) findComponentById(ResourceTable.Id_sum_age_edit);
        Text mResultText = (Text) findComponentById(ResourceTable.Id_tvSum_result);
        mCountBtn1.setClickedListener(component -> {
            result = LitePal.sum(Singer.class, "age", Integer.TYPE);
            mResultText.setText(String.valueOf(result));
        });
        mCountBtn2.setClickedListener(component -> {
            try {
                result = LitePal.where("age > ?", mAgeEdit.getText().toString()).sum(
                        Singer.class, "age", Integer.TYPE);
                mResultText.setText(String.valueOf(result));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
