package com.org.litepal.slice.crud;

import com.org.litepal.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;

public class CrudAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_crud);

        Button btnSave = (Button) findComponentById(ResourceTable.Id_save_sample_btn);
        Button btnUpdate = (Button) findComponentById(ResourceTable.Id_update_sample_btn);
        Button btnDelete = (Button) findComponentById(ResourceTable.Id_delete_sample_btn);
        Button btnQuery = (Button) findComponentById(ResourceTable.Id_query_sample_btn);


        btnSave.setClickedListener(component -> present(new SaveSampleAbilitySlice(),new Intent()));
        btnUpdate.setClickedListener(component -> present(new UpdateSampleAbilitySlice(),new Intent()));
        btnDelete.setClickedListener(component -> present(new DeleteSampleAbilitySlice(),new Intent()));
        btnQuery.setClickedListener(component -> present(new QuerySampleAbilitySlice(),new Intent()));
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
