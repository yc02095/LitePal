package com.org.litepal.slice.manager;

import com.org.litepal.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;

public class ManagerAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_manager);

        Button btnModel = (Button) findComponentById(ResourceTable.Id_btnManager_model);
        Button btnTable =  (Button)findComponentById(ResourceTable.Id_btnManager_table);

        btnModel.setClickedListener(component -> present(new ModelListAbilitySlice(),new Intent()));
        btnTable.setClickedListener(component -> present(new TableListAbilitySlice(),new Intent()));
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
