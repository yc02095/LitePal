package com.org.litepal.slice.manager;

import com.org.litepal.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;
import org.litepal.util.BaseUtility;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

public class ModelStrutureAbilitySlice extends AbilitySlice {
    public static final String CLASS_NAME = "class_name";

    private String mClassName;

    private ArrayList<Field> mList = new ArrayList<>();
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(com.org.litepal.ResourceTable.Layout_model_struture_layout);
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_lvModel_struture);

        mClassName = intent.getStringParam(CLASS_NAME);
        analyzeModelStructure();

        MyItemProvider itemProvider = new MyItemProvider(mList, this.getContext());
        listContainer.setItemProvider(itemProvider);

    }
    private void analyzeModelStructure() {
        Class<?> dynamicClass = null;
        try {
            dynamicClass = Class.forName(mClassName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Field[] fields = new Field[0];
        if (dynamicClass != null) {
            fields = dynamicClass.getDeclaredFields();
        }
        for (Field field : fields) {
            int modifiers = field.getModifiers();
            if (Modifier.isPrivate(modifiers) && !Modifier.isStatic(modifiers)) {
                Class<?> fieldTypeClass = field.getType();
                String fieldType = fieldTypeClass.getName();
                if (BaseUtility.isFieldTypeSupported(fieldType)) {
                    mList.add(field);
                }
            }
        }
    }
    static class MyItemProvider extends RecycleItemProvider {
        private ArrayList<Field> list;
        private Context mContext;
        MyItemProvider(ArrayList<Field> list,Context context){
            this.list = list;
            mContext = context;
        }

        @Override
        public int getCount() {
            return list == null ? 0 : list.size();
        }

        @Override
        public Object getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return list.get(i).hashCode();
        }

        @Override
        public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
            Component c = LayoutScatter.getInstance(mContext).parse(com.org.litepal.ResourceTable.Layout_model_structure_item,componentContainer,false);
            Text text = (Text) c.findComponentById(ResourceTable.Id_tvItem_1);
            Text text2 = (Text) c.findComponentById(ResourceTable.Id_tvItem_2);

            text.setText(list.get(i).getName());
            text2.setText(list.get(i).getType().getName());

            return c;
        }
    }

}
