package com.org.litepal.slice.crud;

import com.org.litepal.ResourceTable;
import com.org.litepal.model.Singer;
import com.org.litepal.provider.DataItemProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;
import ohos.data.resultset.ResultSet;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import org.litepal.tablemanager.Connector;

import java.util.ArrayList;
import java.util.List;

public class UpdateSampleAbilitySlice  extends AbilitySlice {
    private TextField mSingerIdEdit;

    private TextField mSingerNameEdit;

    private TextField mSingerAgeEdit;

    private TextField mNameToUpdateEdit;

    private TextField mAgeToUpdateEdit;

    private RoundProgressBar mProgressBar;

    private DataItemProvider mAdapter;

    private ArrayList<List<String>> mList = new ArrayList<>();
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_update_sample);
        mProgressBar =  (RoundProgressBar)findComponentById(ResourceTable.Id_progressUpdate_bar);
        mSingerIdEdit =  (TextField) findComponentById(ResourceTable.Id_singer_id_edit);
        mSingerNameEdit = (TextField) findComponentById(ResourceTable.Id_singer_name_edit);
        mSingerAgeEdit = (TextField) findComponentById(ResourceTable.Id_singer_age_edit);
        mNameToUpdateEdit = (TextField) findComponentById(ResourceTable.Id_name_to_update);
        mAgeToUpdateEdit = (TextField) findComponentById(ResourceTable.Id_age_to_update);
        Button mUpdateBtn1 = (Button)findComponentById(ResourceTable.Id_update_btn1);
        Button mUpdateBtn2 = (Button)findComponentById(ResourceTable.Id_update_btn2);
        ListContainer mDataListView = (ListContainer) findComponentById(ResourceTable.Id_dataUpdate_list_view);
        mUpdateBtn1.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    Singer singer = new Singer();
                    singer.setName(mSingerNameEdit.getText().toString());
                    singer.setAge(Integer.parseInt(mSingerAgeEdit.getText().toString()));
                    int rowsAffected = singer
                            .update(Long.parseLong(mSingerIdEdit.getText().toString()));
                    ToastDialog dialog = new ToastDialog(getContext());
                    dialog.setText(String.format("%1$s rows affected.",
                            String.valueOf(rowsAffected)));
                    dialog.setDuration(1500);
                    dialog.show();
                    populateDataFromDB();
                } catch (Exception e) {
                    e.printStackTrace();
                    ToastDialog dialog = new ToastDialog(getContext());
                    dialog.setText("Param is not valid.");
                    dialog.setDuration(1500);
                    dialog.show();
                }
            }
        });
        mUpdateBtn2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    Singer singer = new Singer();
                    singer.setName(mSingerNameEdit.getText().toString());
                    singer.setAge(Integer.parseInt(mSingerAgeEdit.getText().toString()));
                    int rowsAffected = singer.updateAll("name=? and age=?", mNameToUpdateEdit.getText()
                            .toString(), mAgeToUpdateEdit.getText().toString());
                    ToastDialog dialog = new ToastDialog(getContext());
                    dialog.setText(String.format("%1$s rows affected.",
                            String.valueOf(rowsAffected)));
                    dialog.setDuration(1500);
                    dialog.show();
                    populateDataFromDB();
                } catch (Exception e) {
                    e.printStackTrace();
                    ToastDialog dialog = new ToastDialog(getContext());
                    dialog.setText("Param is not valid.");
                    dialog.setDuration(1500);
                    dialog.show();
                }
            }
        });
        mAdapter = new DataItemProvider(mList,getContext());
        mDataListView.setItemProvider(mAdapter);
        populateDataFromDB();
    }

    private void populateDataFromDB() {
        mProgressBar.setVisibility(Component.VISIBLE);
        new Thread(new Runnable() {
            @Override
            public void run() {
                mList.clear();
                List<String> columnList = new ArrayList<>();
                columnList.add("id");
                columnList.add("name");
                columnList.add("age");
                columnList.add("ismale");
                mList.add(columnList);
                ResultSet cursor = null;
                try {
                    cursor = Connector.getDatabase().querySql("select * from singer order by id",
                            null);
                    if (cursor.goToFirstRow()) {
                        do {
                            long id = cursor.getLong(cursor.getColumnIndexForName("id"));
                            String name = cursor.getString(cursor.getColumnIndexForName("name"));
                            int age = cursor.getInt(cursor.getColumnIndexForName("age"));
                            int isMale = cursor.getInt(cursor.getColumnIndexForName("ismale"));
                            List<String> stringList = new ArrayList<>();
                            stringList.add(String.valueOf(id));
                            stringList.add(name);
                            stringList.add(String.valueOf(age));
                            stringList.add(String.valueOf(isMale));
                            mList.add(stringList);
                        } while (cursor.goToNextRow());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                    handler.postTask(() -> {
                        mProgressBar.setVisibility(Component.HIDE);
                        mAdapter.notifyDataChanged();
                    });
                }
            }
        }).start();
    }
    private EventHandler handler = new EventHandler(EventRunner.getMainEventRunner());
    @Override
    protected void onStop() {
        super.onStop();
        handler.removeAllEvent();
    }
}