package com.org.litepal.slice.manager;

import com.org.litepal.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;
import org.litepal.tablemanager.Connector;
import org.litepal.tablemanager.model.ColumnModel;
import org.litepal.tablemanager.model.TableModel;
import org.litepal.util.DBUtility;

import java.util.ArrayList;
import java.util.Collection;


public class TableStrutureAbilitySlice extends AbilitySlice {
    static final String TABLE_NAME = "table_name";


    private String mTableName;

    private ArrayList<ColumnModel> mList = new ArrayList<>();
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(com.org.litepal.ResourceTable.Layout_ability_table_struture);
        ListContainer listContainer = (ListContainer) findComponentById(com.org.litepal.ResourceTable.Id_lvTable_struture);

        mTableName = intent.getStringParam(TABLE_NAME);
        analyzeTableStructure();

        MyTableItemProvider itemProvider = new MyTableItemProvider(mList, this.getContext());
        listContainer.setItemProvider(itemProvider);

    }
    private void analyzeTableStructure() {
        TableModel tableMode = DBUtility.findPragmaTableInfo(mTableName, Connector.getDatabase());
        Collection<ColumnModel> columnModelList = tableMode.getColumnModels();
        mList.addAll(columnModelList);

        System.out.println("litepal  -------------> SIZE"+ mList.size() + "     TOSTRING"+mList.toString());
    }
    static class MyTableItemProvider extends RecycleItemProvider {
        private ArrayList<ColumnModel> list;
        private Context mContext;
        MyTableItemProvider(ArrayList<ColumnModel> list,Context context){
            this.list = list;
            mContext = context;
        }

        @Override
        public int getCount() {
            return list == null ? 0 : list.size();
        }

        @Override
        public Object getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return list.get(i).hashCode();
        }

        @Override
        public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
            ColumnModel columnModel = list.get(i);
            String columnName = columnModel.getColumnName();
            String columnType = columnModel.getColumnType();
            boolean nullable = columnModel.isNullable();
            boolean unique = columnModel.isUnique();
            boolean hasIndex = columnModel.hasIndex();
            String defaultValue = columnModel.getDefaultValue();
            Component c = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_table_struture_item,componentContainer,false);

            Text text = (Text) c.findComponentById(ResourceTable.Id_tvTableStru_1);
            Text text2 = (Text) c.findComponentById(ResourceTable.Id_tvTableStru_2);
            Text text3 = (Text) c.findComponentById(ResourceTable.Id_tvTableStru_3);
            Text text4 = (Text) c.findComponentById(ResourceTable.Id_tvTableStru_4);
            Text text5 = (Text) c.findComponentById(ResourceTable.Id_tvTableStru_5);
            Text text6 = (Text) c.findComponentById(ResourceTable.Id_tvTableStru_6);

            text.setText(columnName);
            text2.setText(columnType);
            text3.setText(String.valueOf(nullable));
            text4.setText(String.valueOf(unique));
            text5.setText(defaultValue);
            text6.setText(String.valueOf(hasIndex));

            return c;
        }
    }
}
