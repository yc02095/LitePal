package com.org.litepal.slice.aggregate;

import com.org.litepal.ResourceTable;
import com.org.litepal.slice.crud.DeleteSampleAbilitySlice;
import com.org.litepal.slice.crud.QuerySampleAbilitySlice;
import com.org.litepal.slice.crud.SaveSampleAbilitySlice;
import com.org.litepal.slice.crud.UpdateSampleAbilitySlice;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;

public class AggregateAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_aggregate);


        Button btnCount = (Button) findComponentById(ResourceTable.Id_count_sample_btn);
        Button btnMax = (Button) findComponentById(ResourceTable.Id_max_sample_btn);
        Button btnMin = (Button) findComponentById(ResourceTable.Id_min_sample_btn);
        Button btnAverage = (Button) findComponentById(ResourceTable.Id_average_sample_btn);
        Button btnSum = (Button) findComponentById(ResourceTable.Id_sum_sample_btn);


        btnCount.setClickedListener(component -> present(new CountSampleAbilitySlice(),new Intent()));
        btnMax.setClickedListener(component -> present(new MaxSampleAbilitySlice(),new Intent()));
        btnMin.setClickedListener(component -> present(new MinSampleAbilitySlice(),new Intent()));
        btnAverage.setClickedListener(component -> present(new AverageSampleAbilitySlice(),new Intent()));
        btnSum.setClickedListener(component -> present(new SumSampleAbilitySlice(),new Intent()));
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
