package com.org.litepal.slice;

import com.org.litepal.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.text.Font;
import ohos.bundle.IBundleManager;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.resultset.ResultSet;
import ohos.media.photokit.metadata.AVStorage;

public class MainAbilitySlice extends AbilitySlice {


    private static final int MANAGER = 0;
    private static final int CRUD = 1;
    private static final int AGGREGATE = 2;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Button text = (Button) findComponentById(ResourceTable.Id_btnMain_manager);
        text.setClickedListener(component -> startAbilityManager(MANAGER));
        text.setFont(Font.DEFAULT_BOLD);


        Button crud = (Button) findComponentById(ResourceTable.Id_btnMain_crud);
        crud.setClickedListener(component -> startAbilityManager(CRUD));

        Button aggregate =(Button) findComponentById(ResourceTable.Id_btnMain_aggregate);
        aggregate.setClickedListener(component -> startAbilityManager(AGGREGATE));
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void startAbilityManager(int index){
        Intent secondIntent = new Intent();
        Operation operation;
        switch (index){
            case AGGREGATE:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.org.litepal")
                        .withAbilityName("com.org.litepal.AggregateAbility")
                        .build();
                break;
            case CRUD:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.org.litepal")
                        .withAbilityName("com.org.litepal.CrudAbility")
                        .build();
                break;
            default:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.org.litepal")
                        .withAbilityName("com.org.litepal.ManagerAbility")
                        .build();
                break;
        }
        secondIntent.setOperation(operation);
        startAbility(secondIntent);
    }

}
