package com.org.litepal.slice.aggregate;

import com.org.litepal.ResourceTable;
import com.org.litepal.model.Singer;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import org.litepal.LitePal;

public class MaxSampleAbilitySlice extends AbilitySlice {
    private int result = 0;
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_aggregate_max);

        Button mCountBtn1 = (Button) findComponentById(ResourceTable.Id_max_btn1);
        Button mCountBtn2 = (Button) findComponentById(ResourceTable.Id_max_btn2);
        TextField mAgeEdit = (TextField) findComponentById(ResourceTable.Id_max_age_edit);
        Text mResultText = (Text) findComponentById(ResourceTable.Id_tvMax_result);
        mCountBtn1.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                result = LitePal.max(Singer.class, "age", Integer.TYPE);
                mResultText.setText(String.valueOf(result));
            }
        });
        mCountBtn2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    result = LitePal.where("age > ?", mAgeEdit.getText().toString()).max(
                            Singer.class, "age", Integer.TYPE);
                    mResultText.setText(String.valueOf(result));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
