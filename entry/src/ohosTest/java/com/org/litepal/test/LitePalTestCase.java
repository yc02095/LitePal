package com.org.litepal.test;

import java.util.ArrayList;
import java.util.List;

import ohos.data.rdb.RawRdbPredicates;
import ohos.data.rdb.RdbStore;
import ohos.data.resultset.ResultSet;
import org.litepal.tablemanager.Connector;
import org.litepal.util.BaseUtility;
import org.litepal.util.DBUtility;

import com.org.litepal.model.Book;
import com.org.litepal.model.Cellphone;
import com.org.litepal.model.Classroom;
import com.org.litepal.model.Computer;
import com.org.litepal.model.IdCard;
import com.org.litepal.model.Student;
import com.org.litepal.model.Teacher;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;


public class LitePalTestCase {

	protected void assertM2M(String table1, String table2, long id1, long id2) {
		assertTrue(isIntermediateDataCorrect(table1, table2, id1, id2));
	}

	protected void assertM2MFalse(String table1, String table2, long id1, long id2) {
		assertFalse(isIntermediateDataCorrect(table1, table2, id1, id2));
	}

	/**
	 * 
	 * @param table1
	 *            Table without foreign key.
	 * @param table2
	 *            Table with foreign key.
	 * @param table1Id
	 *            id of table1.
	 * @param table2Id
	 *            id of table2.
	 * @return success or failed.
	 */
	protected boolean isFKInsertCorrect(String table1, String table2, long table1Id, long table2Id) {
		RdbStore db = Connector.getDatabase();
		ResultSet cursor = null;
		try {
			cursor = db.query(new RawRdbPredicates(table2,"id = ?",new String[] { String.valueOf(table2Id) }),null);
			cursor.goToFirstRow();
			long fkId = cursor.getLong(cursor.getColumnIndexForName(BaseUtility.changeCase(table1
					+ "_id")));
			return fkId == table1Id;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	protected boolean isIntermediateDataCorrect(String table1, String table2, long table1Id,
			long table2Id) {
		RdbStore db = Connector.getDatabase();
		ResultSet cursor = null;
		try {
			String where = table1 + "_id = ? and " + table2 + "_id = ?";
			cursor = db.query(new RawRdbPredicates(DBUtility.getIntermediateTableName(table1, table2),where
					,new String[] { String.valueOf(table1Id), String.valueOf(table2Id) }),null);
			return cursor.getRowCount() == 1;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	protected long getForeignKeyValue(String tableWithFK, String tableWithoutFK, long id) {
		ResultSet cursor = Connector.getDatabase().query(new RawRdbPredicates(tableWithFK, "id = ?",
				new String[]{String.valueOf(id)}), null);
		long foreignKeyId = 0;
		if (cursor.goToFirstRow()) {
			foreignKeyId = cursor.getLong(cursor.getColumnIndexForName(BaseUtility
					.changeCase(tableWithoutFK + "_id")));
		}
		cursor.close();
		return foreignKeyId;
	}

	protected boolean isDataExists(String table, long id) {
		RdbStore db = Connector.getDatabase();
		ResultSet cursor = null;
		try {
			cursor = db.query(new RawRdbPredicates(table, "id = ?", new String[] { String.valueOf(id) }),
					 null);
			return cursor.getRowCount() == 1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return false;
	}

	protected String getTableName(Object object) {
		return DBUtility.getTableNameByClassName(object.getClass().getName());
	}

    protected String getTableName(Class<?> c) {
        return DBUtility.getTableNameByClassName(c.getName());
    }

	protected int getRowsCount(String tableName) {
		int count = 0;
		ResultSet c = Connector.getDatabase().query(new RawRdbPredicates(tableName,null,null),null);
		count = c.getRowCount();
		c.close();
		return count;
	}

	protected List<Book> getBooks(String[] columns, String selection, String[] selectionArgs,
			String groupBy, String having, String orderBy, String limit) {
		List<Book> books = new ArrayList<Book>();
//		ResultSet cursor = Connector.getDatabase().query(getTableName(Book.class), columns, selection, selectionArgs,
//				groupBy, having, orderBy, limit);
		ResultSet cursor = Connector.getDatabase().query(new RawRdbPredicates(getTableName(Book.class),selection,selectionArgs),columns);
		if (cursor.goToFirstRow()) {
			do {
				long id = cursor.getLong(cursor.getColumnIndexForName("id"));
				String bookName = cursor.getString(cursor.getColumnIndexForName("bookname"));
				Integer pages = null;
				if (!cursor.isColumnNull(cursor.getColumnIndexForName("pages"))) {
					pages = cursor.getInt(cursor.getColumnIndexForName("pages"));
				}
				double price = cursor.getDouble(cursor.getColumnIndexForName("price"));
				char level = cursor.getString(cursor.getColumnIndexForName("level")).charAt(0);
				short isbn = cursor.getShort(cursor.getColumnIndexForName("isbn"));
				float area = cursor.getFloat(cursor.getColumnIndexForName("area"));
				boolean isPublished = cursor.getInt(cursor.getColumnIndexForName("ispublished")) == 1;
				Book book = new Book();
				book.setId(id);
				book.setBookName(bookName);
				book.setPages(pages);
				book.setPrice(price);
				book.setLevel(level);
				book.setIsbn(isbn);
				book.setArea(area);
				book.setPublished(isPublished);
				books.add(book);
			} while (cursor.goToNextRow());
		}
		cursor.close();
		return books;
	}

	protected Classroom getClassroom(long id) {
		Classroom c = null;
		ResultSet cursor = Connector.getDatabase().query(new RawRdbPredicates(getTableName(Classroom.class), "id = ?",
				new String[] { String.valueOf(id) }), null);
		if (cursor.goToFirstRow()) {
			c = new Classroom();
			String name = cursor.getString(cursor.getColumnIndexForName("name"));
			c.setName(name);
		}
		cursor.close();
		return c;
	}

	protected IdCard getIdCard(long id) {
		IdCard card = null;
		ResultSet cursor = Connector.getDatabase().query(new RawRdbPredicates(getTableName(IdCard.class), "id = ?",
				new String[] { String.valueOf(id) }), null);
		if (cursor.goToFirstRow()) {
			card = new IdCard();
			String address = cursor.getString(cursor.getColumnIndexForName("address"));
			String number = cursor.getString(cursor.getColumnIndexForName("number"));
			card.setAddress(address);
			card.setNumber(number);
		}
		cursor.close();
		return card;
	}

	protected Computer getComputer(long id) {
		Computer computer = null;
		ResultSet cursor = Connector.getDatabase().query(new RawRdbPredicates(getTableName(Computer.class), "id = ?",
				new String[] { String.valueOf(id) }), null);
		if (cursor.goToFirstRow()) {
			computer = new Computer("", 0);
			double newPrice = cursor.getDouble(cursor.getColumnIndexForName("price"));
			String brand = cursor.getString(cursor.getColumnIndexForName("brand"));
			computer.setBrand(brand);
			computer.setPrice(newPrice);
		}
		cursor.close();
		return computer;
	}

	protected Cellphone getCellPhone(long id) {
		Cellphone cellPhone = null;
		ResultSet cursor = Connector.getDatabase().query(new RawRdbPredicates(getTableName(Cellphone.class), "id = ?",
				new String[] { String.valueOf(id) }), null);
		if (cursor.goToFirstRow()) {
			cellPhone = new Cellphone();
			double newPrice = cursor.getDouble(cursor.getColumnIndexForName("price"));
			char inStock = cursor.getString(cursor.getColumnIndexForName("instock")).charAt(0);
			String brand = cursor.getString(cursor.getColumnIndexForName("brand"));
			cellPhone.setBrand(brand);
			cellPhone.setInStock(inStock);
			cellPhone.setPrice(newPrice);
		}
		cursor.close();
		return cellPhone;
	}

	protected Teacher getTeacher(long id) {
		Teacher teacher = null;
		ResultSet cursor = Connector.getDatabase().query(new RawRdbPredicates(getTableName(Teacher.class), "id = ?",
				new String[] { String.valueOf(id) }), null);
		if (cursor.goToFirstRow()) {
			teacher = new Teacher();
			String teacherName = cursor.getString(cursor.getColumnIndexForName("teachername"));
			int teachYears = cursor.getInt(cursor.getColumnIndexForName("teachyears"));
			int age = cursor.getInt(cursor.getColumnIndexForName("age"));
			int sex = cursor.getInt(cursor.getColumnIndexForName("sex"));
			teacher.setTeacherName(teacherName);
			teacher.setTeachYears(teachYears);
			teacher.setAge(age);
			if (sex == 0) {
				teacher.setSex(false);
			} else if (sex == 1) {
				teacher.setSex(true);
			}
		}
		cursor.close();
		return teacher;
	}

	protected Student getStudent(long id) {
		Student student = null;
		ResultSet cursor = Connector.getDatabase().query(new RawRdbPredicates(getTableName(Student.class), "id = ?",
				new String[] { String.valueOf(id) }), null);
		if (cursor.goToFirstRow()) {
			student = new Student();
			String name = cursor.getString(cursor.getColumnIndexForName("name"));
			int age = cursor.getInt(cursor.getColumnIndexForName("age"));
			student.setName(name);
			student.setAge(age);
		}
		cursor.close();
		return student;
	}

	protected List<Teacher> getTeachers(int[] ids) {
		List<Teacher> teachers = new ArrayList<Teacher>();
		ResultSet cursor = Connector.getDatabase().query(new RawRdbPredicates(getTableName(Teacher.class), getWhere(ids), null), null);
		if (cursor.goToFirstRow()) {
			Teacher t = new Teacher();
			String teacherName = cursor.getString(cursor.getColumnIndexForName("teachername"));
			int teachYears = cursor.getInt(cursor.getColumnIndexForName("teachyears"));
			int age = cursor.getInt(cursor.getColumnIndexForName("age"));
			int sex = cursor.getInt(cursor.getColumnIndexForName("sex"));
			t.setTeacherName(teacherName);
			t.setTeachYears(teachYears);
			t.setAge(age);
			if (sex == 0) {
				t.setSex(false);
			} else if (sex == 1) {
				t.setSex(true);
			}
			teachers.add(t);
		}
		cursor.close();
		return teachers;
	}

	protected List<Student> getStudents(int[] ids) {
		List<Student> students = new ArrayList<Student>();
		ResultSet cursor = Connector.getDatabase().query(new RawRdbPredicates(getTableName(Student.class), getWhere(ids), null), null);
		if (cursor.goToFirstRow()) {
			Student s = new Student();
			String name = cursor.getString(cursor.getColumnIndexForName("name"));
			int age = cursor.getInt(cursor.getColumnIndexForName("age"));
			s.setName(name);
			s.setAge(age);
			students.add(s);
		}
		cursor.close();
		return students;
	}

	private String getWhere(int[] ids) {
		StringBuilder where = new StringBuilder();
		boolean needOr = false;
		for (int id : ids) {
			if (needOr) {
				where.append(" or ");
			}
			where.append("id = ").append(id);
			needOr = true;
		}
		return where.toString();
	}

}
