package com.org.litepal.test.util;

import com.org.litepal.model.Book;
import com.org.litepal.model.Cellphone;
import com.org.litepal.test.LitePalTestCase;

import ohos.data.rdb.RdbStore;
import ohos.utils.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.suppliers.TestedOn;
import org.litepal.tablemanager.Connector;
import org.litepal.util.DBUtility;

import java.util.Set;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class DBUtilityTest extends LitePalTestCase {

    RdbStore db;

    @Before
    public void setUp() {
        db = Connector.getDatabase();
    }

    @Test
    public void testFindIndexedColumns() {
        Pair<Set<String>, Set<String>> pair = DBUtility.findIndexedColumns(DBUtility.getTableNameByClassName(Cellphone.class.getName()), db);
        Set<String> indexColumns = pair.f;
        Set<String> uniqueColumns = pair.s;
        assertEquals(1, indexColumns.size());
        assertEquals(1, uniqueColumns.size());
        assertTrue(indexColumns.contains("brand"));
        assertTrue(uniqueColumns.contains("serial"));
        pair = DBUtility.findIndexedColumns(DBUtility.getTableNameByClassName(Book.class.getName()), db);
        indexColumns = pair.f;
        uniqueColumns = pair.s;
        assertEquals(0, indexColumns.size());
        assertEquals(0, uniqueColumns.size());
    }

}
