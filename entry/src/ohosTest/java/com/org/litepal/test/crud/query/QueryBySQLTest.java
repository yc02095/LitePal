package com.org.litepal.test.crud.query;



import com.org.litepal.model.Book;

import ohos.data.resultset.ResultSet;
import org.junit.Before;
import org.junit.Test;
import org.litepal.LitePal;
import org.litepal.exceptions.DataSupportException;
import org.litepal.util.DBUtility;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.fail;


public class QueryBySQLTest {

	private Book book;

    private String bookTable;

	@Before
	public void setUp() {
        bookTable = DBUtility.getTableNameByClassName(Book.class.getName());
		book = new Book();
		book.setBookName("数据库");
		book.setPages(300);
		book.save();
	}

	@Test
	public void testQueryBySQL() {
		ResultSet cursor = LitePal.findBySQL("select * from " + bookTable);
		assertTrue(cursor.getRowCount() > 0);
		cursor.close();
	}

    @Test
	public void testQueryBySQLWithPlaceHolder() {
		ResultSet cursor = LitePal.findBySQL(
				"select * from " + bookTable + " where id = ? and bookname = ? and pages = ?",
				String.valueOf(book.getId()), "数据库", "300");
		assertTrue(cursor.getRowCount() > 0);
		cursor.goToFirstRow();
		String bookName = cursor.getString(cursor.getColumnIndexForName("bookname"));
		int pages = cursor.getInt(cursor.getColumnIndexForName("pages"));
		assertEquals(bookName, "数据库");
		assertEquals(pages, 300);
		cursor.close();
	}

    @Test
	public void testQueryBySQLWithWrongParams() {
		try {
            LitePal.findBySQL("select * from " + bookTable + " where id=? and bookname=? and pages=?",
					String.valueOf(book.getId()), "数据库");
			fail();
		} catch (DataSupportException e) {
			assertEquals("The parameters in conditions are incorrect.", e.getMessage());
		}
		ResultSet cursor = LitePal.findBySQL();
		assertNull(cursor);
		cursor = LitePal.findBySQL();
		assertNull(cursor);
	}

}
